import { actions } from 'xstate'
//import { interpret } from 'xstate/lib/interpreter'
import '../data/node_data'
import { get } from '../data/node_data'
const { assign } = actions


/**
 * Create a state from a data node
 * @param {Object} state state:{node_x:{content}}
 * @param {String} action
 */

function createNode( state = {} ){
  const name = Object.keys(state)[0]
  const { next = "", type = "" } = state[name] || { next: "", type: "" }
  switch(type){

    case 'interaction':    
      return {
        states:{
          interact: makeEventHandler({
            node: name,
            names: ['COMPLETE','INTERACT'],
            target: 'answered',
          }),
          answered: makeDualEventState({
            node: name,
            success: 'end',
            error: 'end',
          }),
          end: { type: 'final' },
        },
        onEntry: 'initState',
        initial: 'interact',
        onDone: state[name].next ? 
          makeOnDone({ targets: { a: next.success, b: next.error }, node: name })
          : { target: '' }
      }


    case 'media_interaction':
      return {
        states:{
          play: makeLinearState({ event: 'INTERACT', target: 'interact' }),
          interact: makeEventHandler({
            node: name,
            names: ['COMPLETE','INTERACT'],
            target: 'answered',
          }),
          answered: makeDualEventState({
            node: name,
            success: 'end',
            error: 'end',
          }),
          end: { type: 'final' },
        },
        initial: 'play',
        onEntry: 'initState',
        onDone: state[name].next ? 
          makeOnDone({ targets: { a: next.success, b: next.error }, node: name })
          : { target: '' }
      }

    case 'media':
      return {
        states:{
          loading: makeLinearState({ event: 'ENDLOAD', target: 'play' }),
          play: makeLinearState({ event: 'INTERACT', target: 'end' }),
          end: { type: 'final' },
        },
        initial: 'loading',
        onEntry: 'initState',
        onDone: { target: state[name].next }
      }

    case 'text':
      return {
        states:{
          show: makeLinearState({event: 'END', target: 'end'}),
          end: { type: 'final'}
        },
        initial:'show',
        onDone: { target: state[name].next }
      }

    default:
      return {}
  }
}
//console.log("createNode", createNode())

/**
 * Create a graph from data
 * @param {Object} data mandatory
 */

function makeGraph( data ) {
  const nodesId = data.nodes ? Object.keys(data.nodes) : []

  const nodes = nodesId.reduce((nodeId, key) => {
    const state = {}
    state[key] = data.nodes[key]
    nodeId[key] = createNode(state)

    return nodeId
  }, {})
  //create an intial node for session handling
  nodes['idle'] = {
    initial:"init",
    onEntry:["start"],
    states: {
      init: makeLinearState({ event: 'PLAY', target: 'end' }),
      end: { type: 'final' },
    },
    onDone: nodesId[0]
  }
  let graph = {
    initial: 'idle',
    //debug
    on:{
      NODE_1:".node_1",
      NODE_2:".node_2",
      NODE_3:".node_3",
      NODE_4:".node_4",
      NODE_5:".node_5",
    },
    states: nodes,
  }

  return graph
}
//console.log("graph default",makeGraph({}))

/**
 * Create a state that reacts to 2 events
 * @param {Object} config
 */

function makeDualEventState(config = {}) {
  const { node = '', success = '', error = '' } = config

  return {
    onEntry: 'initSubstate',
    on: {
      SUCCESS: {
        target: success,
        actions: assign(context =>
          updateContext({ node, context, key: 'isCorrect', value: true })
        ),
      },
      ERROR: {
        target: error,
        actions: assign(context =>
          updateContext({ node, context, key: 'isCorrect', value: false })
        ),
      },
    },
  }
}

/**
 * Creates a state with one entry and one output
 * @param {Object}
 */

function makeLinearState(config = {}) {
  const { event = '', target = '' } = config
  const on = {}

  if(event === "INTERACT"){
    on[event] = {
      target,
      actions: "end"
    }
  }else{
    on[event] = {
      target,
    }
  }


  return { 
    //onEntry: `initState`,
    on 
  }
}
//console.log("linearState", makeLinearState({event: "yolo", target: "pouet"}))

/**
 * Create an onDone object with guard
 * @param {Object} config
 */

function makeOnDone(config = {}) {
  const { targets = { a: '', b: '' }, node = '' } = config

  return [
    {
      target: targets.a,
      cond: (context, event) => {
        return context.values[node].content.isCorrect
      },
    },
    {
      target: targets.b,
      cond: (context, event) => {
        return !context.values[node].content.isCorrect
      },
    },
  ]
}

/**
 * Gets answer in the context,
 * checks if answer is complete,
 * and changes state if it is
 * @param {Object} config
 */

function makeEventHandler(config = {}) {
  const { node = '', names = ['',''], target = '' } = config
  const event = {
    [names[0]]: [
      {
        target,
        cond: (context, event) => isAnswerComplete(context.values,node,event),
      },
    ],
    [names[1]]:[
      {
        actions: assign((context, event) =>
          setContext({ context, event:event.value, node })
        ),
      }
    ]
  }
  return {
    onEntry: [`initSubstate`],
    on: { ...event },
  }
}


export function setContext(config = {}) {
  const { node = '', context = { values: {} }, event = "" } = config
  //if value isn't yet in context
  //if new node context
  if(Object.keys(context.values).length === 0 || 
    !context.values[node]){
    let newContext = {
      ...context.values,
      [node]:{
        content:{
          value:[event]
        }
      }
    }
    context.values = newContext
    return context
  }
  //if context node already exists
  const { value } = context.values[node].content
  const newContext = value.includes(event) ?
                value.filter(e => e!== event) :
                [...value, event]
            
  context.values[node].content.value = newContext 

  return context
}

//let context = {"session":{"idle":""},"values":{"node_2":{"content":{"value":["un oiseau"],"isCorrect":false}},"node_5":{"content":{"value":["un peu"],"isCorrect":false}}}}
//context = {"session":{"idle":""},"values":{}}
//console.log("modified",setContext({node:"node_2",context,event:"pouet"}))

/**
 * Update context on a given node
 * @param {Object} config
 */

function updateContext(config = {}) {
  const { node = '', context = {}, key = '', value = [] } = config
  context.values[node].content[key] = value

  return context
}
//console.log("make dual event state", makeDualEventState())


/**
 * returns a boolean
 * @param {Object} event //
 */

function isAnswerComplete(context = {}, node = '', total = 1) {

  if(context[node] && context[node].content && context[node].content.value)
    return context[node].content.value >= total.value
  return false
}


const graph = makeGraph(get())
export default graph
//export { makeLinearState, makeDualEventState, makeEventHandler }

//gets the last session context and current
// export const lastSession = localStorage.interactiveVideo
//   ? JSON.parse(localStorage.getItem('interactiveVideo'))
//   : { session: {"idle": "" }, values: {} }

// const full_actions = {
//   initState: () => {console.log('init state')},
// }

// export const full_Machine = Machine(
//   graph,
//   { actions: full_actions }
//   //lastSession
// ).withContext(lastSession)

// console.log("full machine",graph,lastSession)

// export const full_service = interpret(full_Machine).onTransition(nextState => {
//   console.log('states:', nextState.value, nextState.context)
// })
