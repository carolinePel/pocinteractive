import React from 'react';
import "../../node_modules/video-react/dist/video-react.css";


export function DevTool (props){
    
    const handleClick = (e) => {
        props.handleTool(e.target.value)
    }
    
    return (
        <button value = {props.value} onClick = {handleClick}>{props.value}</button>
    )
}