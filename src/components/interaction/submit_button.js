import React from 'react'
import PropTypes from 'prop-types'
import './submit_button.css'
import '../../index.css'

export function SubmitButton(props) {
  const { readOnly, handleClick } = props
  const content = readOnly ? 'next' : 'ok'

  return (
    <input
      className = "absoluteCenter vCenterSubmitButton submitBtn"
      onClick = {handleClick}
      type = "submit"
      value = {content}
    />
  )
}
SubmitButton.propTypes = {
  handleClick: PropTypes.func,
  readonly: PropTypes.object,
}


