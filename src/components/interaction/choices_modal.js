import React, { Component, Fragment } from 'react'
import './choices.scss'

export class ChoicesModal extends Component{

    setColor(choice,status){
        if(status === "interact") return "transparent"
        else if( choice.v && choice.checked ) return "green"
        else if( choice.v && !choice.checked ) return "lightGreen"
        else if( !choice.v && choice.checked ) return "red"
        else if( !choice.v && !choice.checked ) return "lightGrey"
        else return "transparent"
    }

    render(){
        const { 
            title, 
            subtitle, 
            choices, 
            status, 
            handleChange, 
        } = this.props

        return(
            <Fragment>
                <h1>{title}</h1>
                <p>{subtitle}</p>
                <ul>
                    {choices.map((choice,index) => {
                        const showResult = this.setColor(choice, status)
                        return <Choice 
                                    choice = {choice} 
                                    color = {showResult}
                                    checked = {choice.checked}
                                    key = {"choice"+ index}
                                    handleChange = {handleChange}
                                    status = {status}
                                />
                    })}
                </ul>
            </Fragment>
        )
    }
}
 


function Choice (props) {
    const { checked, status, handleChange, choice } = props  
    return(
        <label className = {`display_block`} style = {{"backgroundColor":props.color}}>
            {status === "interact" && 
             <input 
                type = "checkbox"
                checked = {checked}
                onChange = {handleChange}
                value = {choice.t}
             />}
            {choice.t}
        </label>
    )
}
