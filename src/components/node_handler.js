import React, { Component } from 'react'
import PlayerContainer from '../components/player_container'
import graph from '../states/states'
import { Machine } from 'xstate'
import { interpret } from 'xstate/lib/interpreter';
import InteractionContainer from './interaction_container';
import {
  getInfo,
  getCurrentData,
  getSelected,
  handleResults,
  isUserRight,
  makeState,
  isOutOfRange
} from '../lib/helpers'
import { DevTool } from './devtool';

export class NodeHandler extends Component{
  constructor(props){
    super(props)
    this.state = {
      lastSession: localStorage.interactiveVideo
                  ? JSON.parse(localStorage.getItem('interactiveVideo'))
                  : { session: {"idle": "" }, values: {} },
      paused: true,
      node:"idle",
      status:"init",
      suspendTime: 0,
    }

    const actions = {
      "start": () => {},
      "initState": () => {
        const { value } = this.interpreter.state
        const { suspendTime = 0 } = this.state
        const name = Object.keys(value)[0]
        const node = getCurrentData(name)
        const [ src, interaction, end, start ] = ['src','interaction','end','start']
                                                 .map( key => { return getInfo(node, key) })
        const seek = isOutOfRange(start, suspendTime)

        if(src || interaction){ 
          this.setState(state => makeState({ src, seek, interaction, start, end, node: name }, state))
        }
        
      },
      "initSubstate": () => {
        const { value, context } = this.interpreter.state
        console.log("you", value,this.interpreter.state)
        const node = Object.keys(value)[0]
        const selected = getSelected(context.values)
        // console.log("yoselected",getSelected,context)
        const computed = handleResults(selected,node)
        const success  = isUserRight(computed)
        //console.log("results",selected,computed, success)
        setTimeout(() => {
            success ? 
            this.interpreter.send("SUCCESS") :
            this.interpreter.send("ERROR")
        },2000)
      },
      "end": (ctx,ev) => this.setState({
        suspendTime: ev.value,
        paused: true,
        seek: false
      })
    }

    const machine = Machine(
      graph,
      { actions }
    ).withContext(this.state.lastSession)
    console.log("machine", graph,machine)

    this.interpreter = interpret(machine)
      .onTransition(nextState => {
        const status = Object.values(nextState.value)[0]
        if(this.state.status !== status) this.setState(state =>({status: status}),()=>console.log(this.state))  
      })

    this.interpreter.start()
  }

  handleClick = () => {
    const { value } = this.interpreter.state
    const status = Object.values(value)[0]
    if(status === "init"){
      this.interpreter.send('PLAY')
    }
    if(status === "loading"){
      this.interpreter.send("ENDLOAD")
    }
    if(status == "play"){
      this.setState(state => ({paused: !state.paused}))
    }
  }

      /**
     * Handle context updates
     */

    handleChange = event => {
      const { value } = event.target
      const { send } = this.interpreter

      send({
              type: "INTERACT",
              value
          }
      )
      //to display selected items
      this.forceUpdate()
  }

  onTimeChange = (playerState, playerPrevState) => {
    const { end, paused } = this.state
    const { currentTime } = playerPrevState
    
    //synchronize player with state
    this.setState(state => {
      if(playerState.paused !== paused) return { paused: !state.paused }
      else return null
    })
    //end node
    if(Math.round(currentTime) === end){
      this.interpreter.send({type: "INTERACT", value: currentTime })
    }
  }

  //DEBUG
  handleDebug = value => {
    this.interpreter.send(value)
  }

  /**
   * DEBUG
   */
  handleValue = value => {

    if (value === 'COMPLETE') {
      this.interpreter.send({
        type: 'COMPLETE',
        value: { answer: ['answer full'], total: 1 },
      })
    } else {
      this.interpreter.send(value)
    }
  }


  render(){
    const { value } = this.interpreter.state
    const{ start, seek, paused, src, node, interaction } = this.state
    const status = Object.values(value)[0]

    return (
      <div>
        <PlayerContainer
          src = {src}
          paused = {paused}
          node = {node}
          start = {start}
          status = {status}
          seek = {seek}
          handlePlayerChange = {this.onTimeChange}
        />
        <DevTool value = "toggleplay" handleTool = {this.handleClick} />

        {(status === "interact" || 
          status === "answered") &&
        <InteractionContainer 
          interaction = {interaction}
          node = {node}
          status = {status}
          send = {this.interpreter.send}
          context = {this.interpreter.state.context}
          handleChange = {this.handleChange}
        />}
        
        {//DEBUG
          Object.keys(graph.on).map(node => {
            return <DevTool key={node} value = {node} handleTool = {this.handleDebug} />
        })}
        <DevTool value="PLAY" handleTool={this.handleValue} />
        <DevTool value="INTERACT" handleTool={this.handleValue} />
        <DevTool value="COMPLETE" handleTool={this.handleValue} />
        <DevTool value="INCOMPLETE" handleTool={this.handleValue} />
        <DevTool value="SUCCESS" handleTool={this.handleValue} />
        <DevTool value="ERROR" handleTool={this.handleValue} />

      </div>
    )
  }
}

