import React, { Fragment, Component } from 'react';
import { 
    handleResults, 
    isUserRight, 
    getSelected 
} from '../lib/helpers'
import { ChoicesModal } from './interaction/choices_modal'
import { SubmitButton } from './interaction/submit_button'
import PropTypes from 'prop-types'

class InteractionContainer extends Component{


    componentDidUpdate(prevProps,prevState){
        const { status, node, context } = this.props
        const { values } = context
        const selected = (values[node] && values[node].content.value) ? 
                            values[node].content.value : 
                            []

        // if(status === "answered"){
        //     this.endAnswered({selected, node})
        // }
    }
    
    /**
     * handle node end => choose next node
     */

    // endAnswered = (config = {}) => {
    //     const { selected, node } = config
    //     const { send } = this.props
    //     const computed = handleResults(selected,node)
    //     const success  = isUserRight(computed)

    //     setTimeout(() => {
    //         success ? 
    //             send("SUCCESS") :
    //             send("ERROR")
    //     },2000)
        //console.log("resultats:", computed, success)
    //}

    /**
     * Handle button click
     */

    endInteraction = value => {
        const { send } = this.props

        send({
                type: "COMPLETE",
                value: { 
                    total: 1 
                }
            }
        )
        //to display errors
        this.forceUpdate()
        
    }



    render(){
        const { interaction, node, status, context } = this.props
        const selected = getSelected(context.values, node)
        const computed = handleResults(selected, node)
        console.log("selected",context.values, node)
        return (
            <Fragment>
                {interaction && 
                <ChoicesModal 
                    {...interaction} 
                    handleChange = {this.props.handleChange} 
                    choices = {computed}
                    status = {status}
                />}
                {interaction && <SubmitButton handleClick = {this.endInteraction}/>}
            </Fragment>
        )
    }
}

InteractionContainer.propTypes = {
    interaction: PropTypes.object,
    current: PropTypes.object,
    service: PropTypes.object
  }
export default InteractionContainer