import React, { Component } from 'react'
import { Player } from 'video-react'
import PropTypes from 'prop-types'
import '../../node_modules/video-react/dist/video-react.css'
import { DevTool } from './devtool'
import {
  isOutOfRange,
  getCurrentData,
  isNodeChange
} from '../lib/helpers'


class PlayerContainer extends Component {

  componentDidMount() {
    this.refs.player.subscribeToStateChange(this.props.handlePlayerChange)
  }

  componentDidUpdate(prevProps,prevState){
    const { paused, start, status, seek } = this.props
    const { player } = this.refs
    if(seek && status === "loading") {
      player.seek(start) 

    }   
    if(prevProps.paused !== paused) paused ? player.pause() : this.play()

  }



  play() {
    //if idle state
    this.refs.player.play()
  }

  pause() {
    this.refs.player.pause()
  }

  render() {
    //console.log("render player")
    return (
      <div>
        <Player 
          ref = "player" 
          playsInline src = {this.props.src} 
        />
      </div>
    )
  }
}

PlayerContainer.propTypes = {
  src: PropTypes.string,
  node: PropTypes.string,
  paused: PropTypes.boolean,

}

export default PlayerContainer
