import React, { Component } from 'react';
import { NodeHandler } from './components/node_handler'
import './app.scss';
import './states/states'

class App extends Component {
  render() {
    return (
      <div>
      <NodeHandler />
      </div>
    )
  }
}

export default App;
