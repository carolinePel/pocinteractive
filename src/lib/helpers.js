import { get } from '../data/node_data'



/**
 * gets the current video source
 * @param {Object} state  
 * @returns {Object}
 */

export function getCurrentData(node = "") {
  const { nodes } = get()
  const currentNode = nodes[node]
  
  return currentNode? currentNode : null
}

/**
 * Create a state if data
 * @param {Object} config 
 * @param {Object} state 
 */

export const makeState = (config = {}, state) => {
  const { src = null, start = null, seek = false, end = null, node = null, interaction = null } = config
  const changes = {}

  if(src && (src !== state.src)) changes["src"] = src 
  if(interaction) changes["interaction"] = interaction
  if(start) changes["start"] = start
  if(end) changes["end"] = end
  if(node) changes["node"] = node
  if(seek) changes["seek"] = seek
  
  return changes
}

/**
 * Get selected itemps for a given node
 * @param {Object} service 
 * @param {String} node 
 */

export const getSelected = (values, node) => {
  const selected = (values[node] && values[node].content && values[node].content.value) ? 
                      values[node].content.value : 
                      []
  return selected
}


/**
 * @param {Object}
 * @param {String}
 * @returns {String}
 */
// export function getInfo (arg = {}){
//   const { node = null, field = "", info = null } = arg

//   if(!node || !node.display || !node.display[field]) return null

//   if(!info) return node.display[field]

//   else return node.display[field][info]
// }

export function getInfo(obj, info) {
  var value
  Object.keys(obj).forEach(k => {
      if (k === info) {
          value = obj[k]
          return true
      }
      if (obj[k] && typeof obj[k] === 'object') {
          value = getInfo(obj[k], info)
          return value !== undefined
      }
  })
  return value;
}



//console.log("yo",findVal(get().nodes["node_1"],"pouet"))
//console.log("get media info", getInfo({node: get().nodes["node_2"],field:"interaction"}))
export function isNodeChange( prevNode, node ){
  return prevNode !== node
}

export function getMediaInfo( node, info ){
  if(!node || !node.display || !node.display.media) return null

  return node.display.media[info]
}
//console.log("get media info",getMediaInfo("node_1","src"))


export function getInteractionInfo(node){
  if(!node || !node.display || !node.display.interaction) return null

  return node.display.interaction
}

/**
 * Checks if value out of interval min-max
 * @param {Number} value 
 * @param {Number} min 
 * @param {Number} max 
 */
export function isOutOfRange (a, b){
  return (Math.abs(Math.round(a) - b) > 2)
}


/**
 * Creates a new array with 
 * which choice was checked
 * @param {Object} data 
 * @returns {Function}
 */

function analyseData(data){

  return (results, current) => {

    const { interaction } = data[current].display

    return computeResults(interaction, results)
  }
}
/**
 * @param {Object} data 
 * @param {Array} results 
 * @returns {Array}
 */

function computeResults(data = { choices: [] }, results){

  return data.choices.map(choice => {
    if(results.includes(choice.t)) {

      return { ...choice, checked: true }
    }
    else return { ...choice, checked: false}
  })
}

export const handleResults = analyseData(get().nodes)


/**
 * Takes the result of analyseData
 * and returns is user is right or not
 * @param {Array} computed 
 * @returns {boolean}
 */
export function isUserRight(computed){
  let isRight = true
  computed.forEach(result => {
    const isCheckedAndFalse = result.checked && !result.v
    const isNotCheckedAndTrue = !result.checked && result.v

    if(isCheckedAndFalse || isNotCheckedAndTrue) 
      isRight = false
  })

  return isRight
}


// export const setCurrent = (state,nextState) => ({ 
//   node: nextState.value 
// })
export const changePaused = state => ({ paused: !state.paused })

export const pause = state => ({ paused : true })

export const play = state => ({ paused: false })