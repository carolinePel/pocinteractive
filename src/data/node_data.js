//import makeEventHandler from '../simple_nested_states'


export function get(){
    return{
        initial:"node_1",
        nodes:{
            node_1: {
                type:"media",
                next: "node_2",
                display: {
                    media: {
                        start:20,
                        end:25,
                        src:"https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                    },
                }, 
            },
            node_2:{
                type:"interaction",
                next: {
                    success: "node_3",
                    error: "node_4"
                },
                display:{
                    interaction: {
                        // Page title (short)
                        title: "Qu'est Dagobert?",
                        type: "unique",
                        // Page explanation (long, markdown)
                        subtitle: `Un seul choix`,
                        choices: [
                            { v: false, t: 'un oiseau' },
                            { v: false, t: 'un prince charmant' },
                            { v: false, t: "une marque de vêtement" },
                            { v: true,  t: 'un roi' },
                        ]
                    }
                }
            },
            node_3:{
                type:"media",
                next: "node_5",
                display:{
                    media:{
                        start: 25,
                        end:30,
                        src:"http://media.w3.org/2010/05/bunny/movie.mp4"
                    },
                }
            },
            node_4:{
                type:"media",
                next: "node_5",
                display:{
                    media:{
                        start: 30,
                        end: 35,
                        src:"http://media.w3.org/2010/05/bunny/movie.mp4"
                    },
                }
            },
            node_5:{
                type:"interaction",
                next:{
                    success: "node_6",
                    error: "node_7"
                },
                display:{
                    interaction: {
                        // Page title (short)
                        title: "Aimez-vous Elisabeth II?",
                        type: "unique",
                        // Page explanation (long, markdown)
                        subtitle: `Un seul choix`,
                        choices: [
                            { v: false, t: 'un peu' },
                            { v: false, t: 'beaucoup' },
                            { v: false, t: "à la folie" },
                            { v: true,  t: 'pas du tout' },
                        ]
                    }
                }
            },
            node_6:{
                type:"media",
                next: "node_5",
                display:{
                    media:{
                        start: 35,
                        end: 40,
                        src:"http://media.w3.org/2010/05/bunny/movie.mp4"
                    },
                }
            },
            node_7:{
                type:"media",
                next: "node_5",
                display:{
                    media:{
                        start: 40,
                        end: 45,
                        src:"http://media.w3.org/2010/05/bunny/movie.mp4"
                    },
                }
            },
            node_8: {
                type:"text",
                next: "",
                display:{
                    text:{
                        title: "Merci de votre participation",
                        content:"J'aime les cerises car on a pas besoin de les éplucher"
                    }
                }
            }
        }
    }
        
}

